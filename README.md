# FLOSS data on OGP Toolbox

Quick analysis of tools and softwares registered on the [OGP Toolbox](https://ogptoolbox.org/) using its [API](https://api.ogptoolbox.org/) to analyze which fields indicate whether tools are actually free software open source or not.

Turns out about 180 entries are documented as open source out of the 1265 registered as of today.

Documentation through all optional fields can be identified via the combination of at least one of the 4 fields "opensource", "License", "Repo" & "SourceCode". "Download" field has too much mess.

```python
# => FLOSS = card["Repo"] or card["SourceCode"] 
# or (card["opensource"].lower() == "yes")
# or (card["License"] and not any(f in card["License"].lower() for f in ["proprietary", "non-free"]))
```
