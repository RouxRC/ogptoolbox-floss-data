#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, json
import requests
import hashlib

for dr in [".cache", "data"]:
    if not os.path.exists(dr):
        os.makedirs(dr)

md5 = hashlib.md5()
def download(url):
    md5.update(url)
    fname = os.path.join(".cache", md5.hexdigest())
    try:
        with open(fname) as f:
            return json.load(f)
    except:
        data = requests.get(url).json()
    with open(fname, "w") as f:
        json.dump(data, f)
    return data

def get_val(values, prop):
    for lang in ["fr", "en", "es"]:
        try:
            return values[prop]["value"][lang]
        except: pass
    return values[prop]["value"]

rooturl = "https://api.ogptoolbox.org/cards?depth=1&show=values&type=software&type=platform&limit=100&offset="
offset = 0
cards = download(rooturl+"0")
todo = cards["count"]
results = []
properties = {}
while todo:
    if offset:
        cards = download(rooturl + str(offset))
    for cid, card in cards["data"]["cards"].items():
        obj = {"id": cid}
        for prop, val in card["properties"].items():
            prop = get_val(cards["data"]["values"], prop)
            val = get_val(cards["data"]["values"], val)
            if prop not in properties:
                properties[prop] = {}
            if type(val) == dict:
                continue
            elif type(val) == list:
                for v in val:
                    if v not in properties[prop]:
                        properties[prop][v] = 0
                    properties[prop][v] += 1
            else:
                if val not in properties[prop]:
                    properties[prop][val] = 0
                properties[prop][val] += 1
            obj[prop] = val
        results.append(obj)
    todo -= len(cards["data"]["cards"])
    offset += 100

with open(os.path.join("data", "all.json"), "w") as f:
    json.dump(results, f)
with open(os.path.join("data", "properties.json"), "w") as f:
    json.dump(properties, f)

def printval(o, f):
    try:
        res = o[f]
    except:
        res = ""
    if type(res) == list:
        res = " | ".join(res)
    return res.encode('utf-8')

fields = ["opensource", "License", "Repo", "SourceCode", "Download"]
with open(os.path.join("data", "platforms-os.csv"), "w") as csvf:
    concerned = []
    for o in results:
        if any(f in o for f in fields):
            concerned.append([printval(o, f) for f in ["Name"] + fields])
    print >> csvf, "Name;" + ";".join(fields)
    for c in sorted(concerned, key=lambda x: (x[1].lower(),x[2].lower(),x[3],x[4]), reverse=True):
        print >> csvf, ";".join(c)

# => FLOSS =
#    "opensource".lower() == "yes"
# or "License" && not any (f in "License".lower() for f in ["proprietary", "non-free"])
# or "Repo" or "SourceCode"

# "Download" field has too much mess
